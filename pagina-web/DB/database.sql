create database quejas;
use quejas;
/*-*/
create table users (
    id_user                     integer auto_increment,
    nombre                      varchar(25),
    ape1                        varchar(30),
    ape2                        varchar(30),
    telf                        int,
    email                       varchar(50),
    username                    varchar(20),
    passwd                      text,
    gender                      char(1),
    primary key (id_user)
);
create table quejas (
    id                          integer auto_increment,
    descripción_queja           varchar(100),
    usuario_queja               integer not null,
    fecha_queja                 datetime,
    primary key (id),
    foreign key (usuario_queja) references users(id_user)
);