const bcrypt = require('bcryptjs');
const express = require('express')
const nunjucks = require('nunjucks');
const mysql = require('mysql');
const session = require('express-session');
const port = 8004;
const hostname = '0.0.0.0';
const app = express();
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin_quejas',
    password: '12345678Aa@',
    database: 'quejas'
});
connection.connect();
// Sentencia para configurar las plantillas del servicio nunchuk.
nunjucks.configure('pagina-web/views', {
    autoescape: true,
    express: app
});
// Solicitud get para hacer aparecer la página principal (con las opiniones de los usuarios incluídas).
app.get("/", function(req, res) {
    connection.query('select * from quejas', (err, rows, fields) => {
    connection.query('select descripción_queja, username, fecha_queja from quejas inner join users on usuario_queja = id_user group by descripción_queja, username, fecha_queja;', (err, rows, fields) => {
        if (err) throw err;
        console.log(rows);
        res.render('home.njk', {quejas: rows});
   });
});
});
// Solicitud post para hacer aparecer la página principal (con las opiniones de los usuarios incluídas).
app.post("/", function(req, res) {
    connection.query('select * from quejas', (err, rows, fields) => {
        connection.query('select descripción_queja, username, fecha_queja from quejas inner join users on usuario_queja = id_user group by descripción_queja, username, fecha_queja;', (err, rows, fields) => {
            if (err) throw err;
            console.log(rows);
            res.render('home.njk', {quejas: rows});
        });
    });
});
// Solicitud get para hacer aparecer la página de registro.
app.get("/register", function(req, res) {
    res.render('register.njk');
});
app.post("/register", function(req, res) {
    res.render('register.njk');
});
// Solicitud que no me acuerdo lo que hacía
app.use(express.urlencoded({ extended: true }));
// Solicitud listen para ver si el servidor se encuentra operativo o no.
app.listen(port, hostname, function() {
    console.log(`Server running at http://${hostname}:${port}/`);
});
// Solicitud para registrar usuarios e introducirlos en la Base de Datos.
app.post("/register/registered", function(req, res) {
    bcrypt.genSalt(10, function(err, salt) {
        if (err) {
            throw err;
        }

        bcrypt.hash(req.body.passwd, salt, function(err, hash) {
            if (err) {
                throw err;
            };
            
            const user = {
                nombre: req.body.name,
                ape1: req.body.lastname_1,
                ape2: req.body.lastname_2,
                telf: req.body.phone,
                email: req.body.email,
                username: req.body.username,
                passwd: hash,
                gender: req.body.gender
            };
            connection.query('INSERT INTO users SET ?', user, function (error, results, fields) {
                if(err) {
                    throw err;
                };
            });
            res.render('registered.njk');
        });
    });
});
// Solicitud para recoger el usuario y la contraseña a la hora de iniciar sesión con forma de cookie e iniciar sesión correctamente.
app.use(session({
    secret: 'mi_clave_secreta',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true } // establecer a true para cookies seguras en producción
  }));
// RELLENO
app.post('/signup/sign', function(req, res, next) {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(req.body.passwd, salt, function(err, hash) {
            if (err) {
                throw err;
            }
            let username = req.body.username;
            hash = req.body.passwd;
            console.log(username);
            if (!hash) {
                // If the password is empty, render an error view
                res.render('signup_error.njk');
            } else {}
            connection.query(`SELECT * FROM users WHERE username = ?`, username, function(err, results, fields) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error interno del servidor');
                } else if (results.length === 0) {
                    // If no user was found in the database, render an error view
                    res.render('signup_error.njk');
                } else {
                    const user = results[0];
                    if (hash === user.hash) {
                        // If the password is correct, store the username in the session and render the signed view
                        req.session.user = user.username;
                        res.render('signed.njk', {username: req.session.user});
                    }
                    else {
                        // If the password is incorrect, render an error view
                        res.render('signup_error.njk');
                    }
                }
            });
        });
        if (err) {
            throw err;
        }
    });
});
// Para ir a la página de mi cuenta desde la página de inicio, una vez hayamos iniciado sesión.
app.get('/signed', function(req, res) {
    res.render('signed.njk');
});
app.post('/signed', function(req, res) {
    res.render('signed.njk');
});
// Para imprimir la página de inicio pero en lugar de iniciar sesión, que lleve a la página de la cuenta.
app.get('/home', function(req, res) {
    res.render('home_signed.njk');
});
app.post('/home', function(req, res) {
    res.render('home_signed.njk');
});
// Solicitud para hacer aparecer la página de iniciar sesión.
app.get("/signup", function(req, res) {
    const token = generateToken(res, req);
    res.render('signup.njk', {csrfToken: token});
});
app.post("/signup", function(req, res) {
    //const token = generateToken(res, req);
    res.render('signup.njk'/*, {csrfToken: token}*/);
});
// Solicitud para imprimir la queja en pantalla.
app.post("/successfully", function (req, res){
    let queja = req.body.queja[0];
    let username = req.body.queja[1];
    let thisDate = new Date();
//    thisDate = thisDate.getDate();
    // Query para seleccionar todos los datos del usuario que se escoja.
        // Sentencia para introducir nuevos datos.
        let sql = `insert into quejas set ?`;
        let values = {
            descripción_queja: req.body.queja[0],
            usuario_queja: username,
            fecha_queja: thisDate
        };
        // Solicitud para comprobación de errores.
        connection.query(sql, values, function(err, res, fields) {
            if(err) {
                throw err;
            }
        });
    // Solicitud para realizar consulta de selección a la base de datos.
    connection.query('select * from quejas', (err, rows, fields) => {
        connection.query('select descripción_queja, username, fecha_queja from quejas inner join users on usuario_queja = id_user group by descripción_queja, username, fecha_queja;', (err, rows, fields) => {
            if (err) throw err;
            console.log(rows);
            res.render('successfully.njk', {quejas: rows});
        });
    });
});